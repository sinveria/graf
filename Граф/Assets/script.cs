﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using System;

public class script : MonoBehaviour
{
    //public Graf graf;
    public float rastoyanie = 0.5f;
    public GameObject Verschina;
    public GameObject text;
    public int schet;
    public LineRenderer line;
    public int kolvo;
    public GameObject go;
    private bool f1 = true;
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        clickButton();
      
    }

    void clickButton()
    {
        
        Vector3 mousePos;
        if (Input.GetMouseButtonUp(0))
        {
            mousePos = Camera.main.ScreenToWorldPoint(Input.mousePosition);
            mousePos.z = 0;
            go = Instantiate(Verschina);
            go.transform.SetParent(Verschina.transform.parent, true);
            go.transform.localScale = Verschina.transform.localScale;
            go.SetActive(true);
            go.transform.position = new Vector3(mousePos.x, mousePos.y, mousePos.z);
            schet += 1;
            go.transform.name = schet.ToString();
            go.transform.GetChild(0).transform.GetComponent<Text>().text = schet.ToString();
            Graf.Add(schet);
        }

        else if (Input.GetMouseButtonUp(1))
        {
            
            mousePos = Camera.main.ScreenToWorldPoint(Input.mousePosition);
            for (int i = 0; i < this.transform.childCount; i++)
             {
                Debug.Log(this.transform.GetChild(i).gameObject.name);
                GameObject gam = this.transform.GetChild(i).gameObject;
                Vector3 vec = gam.transform.position;
                if((mousePos.x < vec.x + rastoyanie) && (mousePos.y < vec.y + rastoyanie) && (mousePos.x > vec.x - rastoyanie) && (mousePos.y > vec.y  - rastoyanie))
                {
                    if (f1 == true)
                    {
                        gam.GetComponent<Image>().color = Color.red;
                        kolvo = i;
                        f1 = false;
                    }
                    else
                    {
                        if (this.transform.GetChild(kolvo).gameObject.name == gam.name)
                        {
                            gam.transform.GetComponent<Image>().color = Color.white;
                            this.transform.GetChild(kolvo).transform.GetComponent<Image>().color = Color.white;
                            f1 = true;
                        }
                        else if (this.transform.GetChild(kolvo).gameObject.name != gam.name)
                        {
                            line = new GameObject("Line").AddComponent<LineRenderer>();
                            line.startColor = Color.black;
                            line.endColor = Color.black;
                            line.startWidth = 0.1f;
                            line.endWidth = 0.1f;
                            line.positionCount = 2;
                            line.useWorldSpace = true;
                            line.SetPosition(0, this.transform.GetChild(kolvo).gameObject.transform.position);
                            line.SetPosition(1, gam.transform.position);
                            gam.transform.GetComponent<Image>().color = Color.white;
                            this.transform.GetChild(kolvo).transform.GetComponent<Image>().color = Color.white;
                            f1 = true;
                            Graf.Rebro(Convert.ToInt32(this.transform.GetChild(kolvo).gameObject.transform.name), Convert.ToInt32(gam.name));
                        }
                        
                    }
                }

            }

        }
    }

    public void FindWay()
    {
        int a = 3;
        int b = 4;
        Graf.FindWay(a,b); 
    }
}
