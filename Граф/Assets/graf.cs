﻿using System;
using System.Collections.Generic;
using UnityEngine;

static public class Graf 
{
    static public void FindWay(int nachalo, int konec)
    {
        PoiskPuty(nachalo,konec) ;
        Raschari(konec);
        foreach (List<int> a in put)
        {
            foreach(int b in a)
            {
                Console.WriteLine(" {0}, ", b.ToString());
            }
        }
       // Debug.Log("----------------");
    }
    static List<List<int>> spisok = new List<List<int>>();

    static public void Add(int v)
    {
        Debug.Log("Добавить вершину!" + v.ToString());
        spisok.Add(new List<int>());
        Debug.Log(spisok.Count);
    }
    static public void Rebro(int v1, int v2)
    {
        Debug.Log("Добавить ребро! - между" + v1.ToString() + " и " + v2.ToString());
        v1--;
        v2--;
        spisok[v1].Add(v2);
        spisok[v2].Add(v1);
        foreach (List<int> l in spisok)
        {
            string s = "";
            foreach(int i in l)
            {
                s = s + " " + i.ToString();
            }
           // Debug.Log("-" + s);
        }
    }


    static List<List<int>> put = new List<List<int>>();
    static bool f;

    public static void PoiskPuty(int nachalo, int konec)
    {

        put.Add(new List<int> { nachalo });
        do
        {
            Raschari(konec);
        } while (f);

    }

    public static void Raschari(int konec)
    {
        f = false;
        List<List<int>> newput = new List<List<int>>();
        foreach (List<int> p in put)
        {
            int index = p[p.Count - 1]; //получить послдний эл в списке пути
            foreach (int s in spisok[index]) //получить соседей последнего соседа
            {
                if (index == konec)
                {
                    if (!newput.Contains(p)) newput.Add(p);
                }
                else if (!p.Contains(s))
                {
                    List<int> p1 = p.GetRange(0, p.Count);
                    p1.Add(s);
                    newput.Add(p1);
                    f = true;
                }
            }
            put = newput;
        }

    }
    
}
